package fr.avatarreturns.conquerant.utils;

import org.bukkit.configuration.file.YamlConfiguration;

public final class FileUtils {

    public static boolean addDefault(final YamlConfiguration config, final String path, final Object o) {
        if (config.get(path) == null) {
            config.set(path, o);
            return true;
        }
        return false;
    }


}
