package fr.avatarreturns.conquerant.utils;

import com.google.gson.JsonParser;
import org.bukkit.Bukkit;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.Optional;
import java.util.UUID;

public final class OtherUtils {

    public static Optional<UUID> fromString(final String name) {
        try {
            return Optional.of(UUID.fromString(name));
        } catch (Exception e) {
            try {
                final StringBuilder fullUuid = new StringBuilder();
                for (int index = 0; index < name.toCharArray().length; index++) {
                    fullUuid.append(name.charAt(index));
                    if (index == 7 || index == 11 || index == 15 || index == 19)
                        fullUuid.append("-");
                }
                return Optional.of(UUID.fromString(fullUuid.toString()));
            } catch (Exception e2) {
                try {
                    final URL urlToGetUUID = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
                    final InputStreamReader readerUUID = new InputStreamReader(urlToGetUUID.openStream());
                    final String output = new JsonParser().parse(readerUUID).getAsJsonObject().get("id").getAsString();
                    final StringBuilder fullUuid = new StringBuilder();
                    for (int index = 0; index < output.toCharArray().length; index++) {
                        fullUuid.append(output.charAt(index));
                        if (index == 7 || index == 11 || index == 15 || index == 19)
                            fullUuid.append("-");
                    }
                    return Optional.of(UUID.fromString(fullUuid.toString()));
                }
                catch (Exception ex3) {
                    return Optional.empty();
                }
            }
        }
    }

    public static String getName(final Optional<UUID> uuid) {
        if (uuid.isPresent())
            return Bukkit.getOfflinePlayer(uuid.get()).getName();
        return "CONSOLE";
    }

}
