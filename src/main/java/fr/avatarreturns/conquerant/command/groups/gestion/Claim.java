package fr.avatarreturns.conquerant.command.groups.gestion;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.entities.territory.CLand;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import fr.avatarreturns.conquerant.events.group.GroupClaimEvent;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class Claim implements SubCommand {

    @Override
    @Command(command = "groups claim", usage = "{label} {command} [<group>]", executor = Command.ExecutorType.PLAYERS)
    public <T> boolean run(CommandSender sender, List<String> args, T value) {
        assert sender instanceof Player;
        final CPlayer player = CAdapter.adapt((Player) sender);
        if (player.isOverride() && args.size() >= 1) {
            final Optional<Group> group = Group.getGroupByName(args.get(0));
            if (group.isEmpty()) {
                Log.message(sender, Message.ERROR_COMMAND_NEXISTS.get(args.get(0)));
                return false;
            }
            return group.filter(group1 -> claim(player, group1)).isPresent();
        }
        final Optional<Pair<Group, ClassicMember>> member = Group.getGroupByPlayer(player);
        if (member.isPresent()) {
            if (member.get().getSecond().getRank().canClaim() || player.isOverride())
                return claim(player, member.get().getFirst());
            Log.message(sender, Message.ERROR_COMMAND_PERMISSION);
            return false;
        }
        Log.message(sender, Message.ERROR_GROUP_MEMBER);
        return false;
    }

    @Override
    public List<String> tabCompleter(CommandSender sender, List<String> args) {
        return null;
    }

    private boolean claim(final CPlayer player, final Group group) {
        final Optional<Pair<CProvince, CLand>> land = CProvince.getLand(CAdapter.adapt(CAdapter.adapt(player).getLocation()));
        if (land.isEmpty()) {
            Log.message(CAdapter.adapt(player), Message.ERROR_COMMAND_EXEC_CLAIM_LAND_INSIDE);
            return false;
        } else if (land.get().getSecond().getStatus() == CLand.RegionStatus.OCCUPIED) {
            Log.message(CAdapter.adapt(player), Message.ERROR_COMMAND_EXEC_CLAIM_LAND_CLAIMED);
            return false;
        }
        final GroupClaimEvent event = new GroupClaimEvent(player, group, land.get().getFirst(), land.get().getSecond());
        ConquerantPlugin.get().getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            Log.message(CAdapter.adapt(player), Message.ERROR_COMMAND_UNKNOWN);
            return false;
        }
        group.getRegions().add(land.get().getSecond().getUniqueId());
        land.get().getSecond().setStatus(CLand.RegionStatus.OCCUPIED);
        Log.message(CAdapter.adapt(player), Message.INFO_COMMAND_EXEC_CLAIM.get(land.get().getSecond().getName()));
        return true;
    }
}
