package fr.avatarreturns.conquerant.command.groups.gestion;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.Nation;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.events.group.GroupCreateEvent;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class Create implements SubCommand {

    @Override
    @Command(command = "groups create", usage = "{label} {command} <name> [<type>]", arguments = 1)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        if (Group.getGroupByName(args.get(0)).isPresent()) {
            Log.message(sender, Message.ERROR_GROUP_EXISTS.get(args.get(0)));
            return false;
        }
        final Group.GroupType type = args.size() >= 2 ? Group.GroupType.getTypeByValue(args.get(1)) : Group.GroupType.NATION;
        if (sender instanceof Player) {
            final List<Group> groups = Group.getGroupsWithPlayer(CAdapter.adapt((Player) sender));
            if (groups.stream().anyMatch(group -> group.getType() == type) && !CAdapter.adapt((Player) sender).isOverride()) {
                Log.message(sender, Message.ERROR_GROUP_TYPE.get(type.getValue()));
                return false;
            }
        }
        final Group group = new Nation(args.get(0));
        final GroupCreateEvent event = new GroupCreateEvent(sender instanceof Player ? Optional.of(CAdapter.adapt((Player) sender)) : Optional.empty(), group);
        ConquerantPlugin.get().getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            Log.message(sender, Message.ERROR_COMMAND_UNKNOWN);
            return false;
        }
        if (sender instanceof Player) {
            final ClassicMember member = new ClassicMember(((Player) sender).getUniqueId());
            group.getMembers().add(member);
            group.setLeader(member);
        }
        Group.getGroups().add(group);
        Log.message(sender, Message.INFO_COMMAND_EXEC_GROUP_CREATE.get(args.get(0)));
        return true;
    }

    @Override
    public List<String> tabCompleter(final CommandSender sender, final List<String> args) {
        return null;
    }
}
