package fr.avatarreturns.conquerant.command.groups.gestion;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.events.group.GroupKickEvent;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.utils.OtherUtils;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class Kick implements SubCommand {

    @Override
    @Command(command = { "groups kick", "kick" }, usage = "{label} {command} <player> [<group>]", arguments = 1)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        if (!(sender instanceof Player) || CAdapter.adapt((Player) sender).isOverride() && args.size() >= 2) {
            if (args.size() == 1) {
                Log.message(sender, Message.ERROR_COMMAND_ARGUMENTS.get("/conquerant kick <player> <group>", "1"));
                return false;
            }
            final Optional<UUID> playerId = OtherUtils.fromString(args.get(0));
            if (playerId.isEmpty()) {
                Log.message(sender, Message.ERROR_PLAYER_NEXISTS.get(args.get(0)));
                return false;
            }
            final Optional<CPlayer> player = CPlayer.getPlayer(playerId.get());
            if (player.isEmpty()) {
                Log.message(sender, Message.ERROR_PLAYER_NEXISTS.get(args.get(0)));
                return false;
            }
            final OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(playerId.get());
            if (sender instanceof Player) {
                if (((Player) sender).getUniqueId().equals(playerId.get())) {
                    Log.message(sender, Message.ERROR_PLAYER_SELF);
                    return false;
                }
            }
            final Optional<Group> group = Group.getGroupByName(args.get(1));
            if (group.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_NEXISTS.get(args.get(1)));
                return false;
            }
            final Optional<ClassicMember> member = group.get().getMemberWithId(playerId.get());
            if (member.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_PLAYER_NMEMBER.get(offlinePlayer.getName(), group.get().getName()));
                return false;
            }
            final GroupKickEvent event = new GroupKickEvent(Pair.from(player.get(), member.get()), group.get(), true, sender instanceof Player ? Optional.of(((Player) sender).getUniqueId()) : Optional.empty());
            Bukkit.getPluginManager().callEvent(event);
            if (event.isCancelled()) {
                Log.message(sender, Message.ERROR_COMMAND_UNKNOWN);
                return false;
            }
            group.get().getMembers().remove(member.get());
            Log.message(sender, Message.INFO_COMMAND_EXEC_KICK_SEND.get(offlinePlayer.getName(), group.get().getName()));
            if (offlinePlayer.isOnline())
                Log.message(offlinePlayer.getPlayer(), Message.INFO_COMMAND_EXEC_KICK_RECEIVE.get(sender.getName(), group.get().getName()));
            return true;
        }
        final CPlayer host = CAdapter.adapt((Player) sender);
        final Optional<UUID> playerId = OtherUtils.fromString(args.get(0));
        if (playerId.isEmpty()) {
            Log.message(sender, Message.ERROR_PLAYER_NEXISTS.get(args.get(0)));
            return false;
        }
        final Optional<CPlayer> player = CPlayer.getPlayer(playerId.get());
        if (player.isEmpty()) {
            Log.message(sender, Message.ERROR_PLAYER_NEXISTS.get(args.get(0)));
            return false;
        }
        final OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(playerId.get());
        if (((Player) sender).getUniqueId().equals(playerId.get())) {
            Log.message(sender, Message.ERROR_PLAYER_SELF);
            return false;
        }
        final List<Group> groups = Group.getGroupsWithPlayer(host);
        if (groups.size() == 0 && args.size() == 1) {
            Log.message(sender, Message.ERROR_GROUP_MEMBER);
            return false;
        }
        Group group = null;
        boolean groupInArg = args.size() >= 2;
        if (groups.size() == 1) {
            group = groups.get(0);
        } else if (groupInArg) {
            final Optional<Group> optionalGroup = Group.getGroupByName(args.get(1));
            if (optionalGroup.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_NEXISTS.get(args.get(1)));
                return false;
            } if (!groups.contains(optionalGroup.get()) && !host.isOverride() ||
                    !optionalGroup.get().getMemberWithId(((Player) sender).getUniqueId()).get().getRank().canClaim() && !host.isOverride()
            ) {
                Log.message(sender, Message.ERROR_COMMAND_PERMISSION);
                return false;
            }
            group = optionalGroup.get();
        }
        if (group == null) {
            Log.message(sender, Message.ERROR_COMMAND_EXEC_INVITE_PRECISE);
            return false;
        }
        final Optional<ClassicMember> executor = group.getMemberWithId(host.getUniqueId());
        final Optional<ClassicMember> member = group.getMemberWithId(playerId.get());
        if (member.isEmpty()) {
            Log.message(sender, Message.ERROR_GROUP_PLAYER_NMEMBER.get(offlinePlayer.getName(), group.getName()));
            return false;
        }
        if (!host.isOverride() && (executor.isEmpty() || member.get().getRank().getPower() >= executor.get().getRank().getPower())) {
            Log.message(sender, Message.ERROR_GROUP_RANK_POWER_INF.get());
            return false;
        }
        final GroupKickEvent event = new GroupKickEvent(Pair.from(player.get(), member.get()), group, executor.isEmpty() && host.isOverride(), Optional.of(((Player) sender).getUniqueId()));
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            Log.message(sender, Message.ERROR_COMMAND_UNKNOWN);
            return false;
        }
        group.getMembers().remove(member.get());
        Log.message(sender, Message.INFO_COMMAND_EXEC_KICK_SEND.get(offlinePlayer.getName(), group.getName()));
        if (offlinePlayer.isOnline())
            Log.message(offlinePlayer.getPlayer(), Message.INFO_COMMAND_EXEC_KICK_RECEIVE.get(sender.getName(), group.getName()));
        return true;
    }

    @Override
    public List<String> tabCompleter(final CommandSender sender, final List<String> args) {
        return null;
    }
}
