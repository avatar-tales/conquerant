package fr.avatarreturns.conquerant.command.groups;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.logs.Log;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class Leave implements SubCommand {
    @Override
    @Command(command = { "groups leave", "leave" }, usage = "{label} {command} [<group>]", executor = Command.ExecutorType.PLAYERS)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        assert sender instanceof Player;
        final List<Group> groups = Group.getGroupsWithPlayer(CAdapter.adapt((Player) sender));
        if (groups.size() == 0) {
            Log.message(sender, Message.ERROR_GROUP_MEMBER);
            return false;
        } if (groups.size() == 1) {
            if (args.size() >= 1) {
                if (groups.get(0).getName().equalsIgnoreCase(args.get(0))) {
                    return leaveCommonPart(sender, groups);
                }
                Log.message(sender, Message.ERROR_GROUP_NINSIDE.get(groups.get(0).getName()));
                return false;
            }
            return leaveCommonPart(sender, groups);
        } else {
            final Optional<Group> group = groups.stream().filter(allGroups -> allGroups.getName().equalsIgnoreCase(args.get(0))).findFirst();
            if (group.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_NINSIDE.get(groups.get(0).getName()));
                return false;
            }
            final Optional<ClassicMember> member = group.get().getMemberWithId(((Player) sender).getUniqueId());
            if (member.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_NINSIDE.get(group.get().getName()));
                return false;
            }
            group.get().getMembers().remove(member.get());
            Log.message(sender, Message.INFO_COMMAND_EXEC_LEAVE_SELF.get(group.get().getName()));
            for (final ClassicMember members : group.get().getClassicMembers()) {
                final OfflinePlayer player = Bukkit.getOfflinePlayer(members.getUniqueId());
                if (player.isOnline())
                    Log.message(player.getPlayer(), Message.INFO_COMMAND_EXEC_LEAVE_OTHER.get(sender.getName(), group.get().getName()));
            }
        }
        return true;
    }

    private boolean leaveCommonPart(final CommandSender sender, final List<Group> groups) {
        final Optional<ClassicMember> member = groups.get(0).getMemberWithId(((Player) sender).getUniqueId());
        if (member.isEmpty()) {
            Log.message(sender, Message.ERROR_GROUP_NINSIDE.get(groups.get(0).getName()));
            return false;
        }
        groups.get(0).getMembers().remove(member.get());
        Log.message(sender, Message.INFO_COMMAND_EXEC_LEAVE_SELF.get(groups.get(0).getName()));
        for (final ClassicMember members : groups.get(0).getClassicMembers()) {
            final OfflinePlayer player = Bukkit.getOfflinePlayer(members.getUniqueId());
            if (player.isOnline())
                Log.message(player.getPlayer(), Message.INFO_COMMAND_EXEC_LEAVE_OTHER.get(sender.getName(), groups.get(0).getName()));
        }
        return true;
    }

    @Override
    public List<String> tabCompleter(final CommandSender sender, final List<String> args) {
        return null;
    }
}
