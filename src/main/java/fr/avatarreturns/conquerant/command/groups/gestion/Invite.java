package fr.avatarreturns.conquerant.command.groups.gestion;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.utils.OtherUtils;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class Invite implements SubCommand {

    @Override
    @Command(command = "groups invite", usage = "{label} {command} <player> [<group>]", arguments = 1)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        if (!(sender instanceof Player) || CAdapter.adapt((Player) sender).isOverride() && args.size() >= 2) {
            if (args.size() == 1) {
                Log.message(sender, Message.ERROR_COMMAND_ARGUMENTS.get("/conquerant invite <player> <group>", "1"));
                return false;
            }
            final Optional<UUID> playerId = OtherUtils.fromString(args.get(0));
            if (playerId.isEmpty()) {
                Log.message(sender, Message.ERROR_PLAYER_NEXISTS.get(args.get(0)));
                return false;
            }
            final OfflinePlayer bukkitPlayer = Bukkit.getOfflinePlayer(playerId.get());
            if (!bukkitPlayer.isOnline()) {
                Log.message(sender, Message.ERROR_PLAYER_ONLINE.get(args.get(0)));
                return false;
            }
            if (sender instanceof Player) {
                if (sender.equals(bukkitPlayer.getPlayer())) {
                    Log.message(sender, Message.ERROR_PLAYER_SELF);
                    return false;
                }
            }
            final Optional<Group> group = Group.getGroupByName(args.get(1));
            if (group.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_NEXISTS.get(args.get(1)));
                return false;
            }
            if (group.get().getMemberWithId(playerId.get()).isPresent()) {
                return false;
            }
            final CPlayer player = CAdapter.adapt(Objects.requireNonNull(bukkitPlayer.getPlayer()));
            if (player.getPendingInvitations().containsKey(group.get())) {
                Log.message(sender, Message.ERROR_COMMAND_EXEC_INVITE_ALREADY.get(args.get(1), args.get(0)));
                return false;
            }
            player.getPendingInvitations().put(group.get(),
                    Pair.from(
                            sender instanceof Player ? Optional.of(((Player) sender).getUniqueId()) : Optional.empty(),
                            System.currentTimeMillis()
                    )
            );
            Log.message(sender, Message.INFO_COMMAND_EXEC_INVITE_SEND.get(args.get(1), args.get(0)));
            Log.message(bukkitPlayer.getPlayer(), Message.INFO_COMMAND_EXEC_INVITE_RECEIVE.get(sender.getName(), group.get().getName()));
            return true;
        }
        final CPlayer host = CAdapter.adapt((Player) sender);
        final Optional<UUID> playerId = OtherUtils.fromString(args.get(0));
        if (playerId.isEmpty()) {
            Log.message(sender, Message.ERROR_PLAYER_NEXISTS.get(args.get(0)));
            return false;
        }
        final OfflinePlayer bukkitPlayer = Bukkit.getOfflinePlayer(playerId.get());
        if (!bukkitPlayer.isOnline()) {
            Log.message(sender, Message.ERROR_PLAYER_ONLINE.get(args.get(0)));
            return false;
        }
        if (sender.equals(bukkitPlayer.getPlayer())) {
            Log.message(sender, Message.ERROR_PLAYER_SELF);
            return false;
        }
        final List<Group> groups = Group.getGroupsWithPlayer(host);
        if (groups.size() == 0 && args.size() == 1) {
            Log.message(sender, Message.ERROR_GROUP_MEMBER);
            return false;
        }
        Group group = null;
        boolean groupInArg = args.size() >= 2;
        if (groups.size() == 1) {
            group = groups.get(0);
        } else if (groupInArg) {
            final Optional<Group> optionalGroup = Group.getGroupByName(args.get(1));
            if (optionalGroup.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_NEXISTS.get(args.get(1)));
                return false;
            } if (!groups.contains(optionalGroup.get()) && !host.isOverride() ||
                    !optionalGroup.get().getMemberWithId(((Player) sender).getUniqueId()).get().getRank().canInvite() && !host.isOverride()
            ) {
                Log.message(sender, Message.ERROR_COMMAND_PERMISSION);
                return false;
            }
            group = optionalGroup.get();
        }
        if (group == null) {
            Log.message(sender, Message.ERROR_COMMAND_EXEC_INVITE_PRECISE);
            return false;
        }
        final CPlayer player = CAdapter.adapt(Objects.requireNonNull(bukkitPlayer.getPlayer()));
        if (player.getPendingInvitations().containsKey(group)) {
            Log.message(sender, Message.ERROR_COMMAND_EXEC_INVITE_ALREADY.get(args.get(0), group.getName()));
            return false;
        }
        player.getPendingInvitations().put(group,
                Pair.from(
                        Optional.of(((Player) sender).getUniqueId()),
                        System.currentTimeMillis()
                )
        );
        Log.message(sender, Message.INFO_COMMAND_EXEC_INVITE_SEND.get(args.get(0), group.getName()));
        Log.message(bukkitPlayer.getPlayer(), Message.INFO_COMMAND_EXEC_INVITE_RECEIVE.get(sender.getName(), group.getName()));
        return true;
    }

    @Override
    public List<String> tabCompleter(CommandSender sender, List<String> args) {
        return null;
    }
}
