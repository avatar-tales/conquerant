package fr.avatarreturns.conquerant.command.groups;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.events.player.PlayerJoinGroupEvent;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import fr.avatarreturns.conquerant.utils.OtherUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class Join implements SubCommand {

    @Override
    @Command(command = { "groups join", "join" }, usage = "{label} {command} <group> [<player>]", arguments = 1)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        if (!(sender instanceof Player) || CAdapter.adapt((Player) sender).isOverride() && args.size() >= 2) {
            if (args.size() == 1) {
                Log.message(sender, Message.ERROR_COMMAND_ARGUMENTS.get("/conquerant groups join <group> [<player>]", "1"));
                return false;
            }
            final Optional<Group> group = Group.getGroupByName(args.get(0));
            if (group.isEmpty()) {
                Log.message(sender, Message.ERROR_GROUP_NEXISTS.get(args.get(0)));
                return false;
            }
            final Optional<UUID> playerId = OtherUtils.fromString(args.get(1));
            if (playerId.isEmpty()) {
                Log.message(sender, Message.ERROR_PLAYER_NEXISTS.get(args.get(1)));
                return false;
            }
            final OfflinePlayer bukkitPlayer = Bukkit.getOfflinePlayer(playerId.get());
            if (!bukkitPlayer.isOnline()) {
                Log.message(sender, Message.ERROR_PLAYER_ONLINE.get(args.get(1)));
                return false;
            }
            final CPlayer player = CAdapter.adapt(Objects.requireNonNull(bukkitPlayer.getPlayer()));
            return join(group.get(), player, true);
        }
        final Optional<Group> group = Group.getGroupByName(args.get(0));
        if (group.isEmpty()) {
            Log.message(sender, Message.ERROR_GROUP_NEXISTS.get(args.get(0)));
            return false;
        }
        final CPlayer player = CAdapter.adapt((Player) sender);
        if (!group.get().isOpen() && !player.getPendingInvitations().containsKey(group.get())) {
            Log.message(sender, Message.ERROR_PLAYER_INVITATIONS.get(args.get(0)));
            return false;
        }
        return join(group.get(), player, false);
    }

    private boolean join(final Group group, final CPlayer player, final boolean byOperator) {
        final boolean canJoin = Group.getGroupsWithPlayer(player).stream()
                .noneMatch(groups -> groups.getType().equals(group.getType()));
        if (!canJoin && !byOperator) {
            Log.message(CAdapter.adapt(player), Message.ERROR_GROUP_TYPE.get(group.getType().getValue()));
            return false;
        }
        final boolean withInvitation = player.getPendingInvitations().remove(group) != null;
        final PlayerJoinGroupEvent event = new PlayerJoinGroupEvent(player, group, byOperator, withInvitation);
        event.setCancelled(!canJoin);
        ConquerantPlugin.get().getServer().getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            final ClassicMember member = new ClassicMember(player.getUniqueId());
            member.setRank(Group.Ranks.RECRUIT);
            group.getMembers().add(member);
            for (final ClassicMember members : group.getMembers().stream()
                    .filter(check -> check instanceof ClassicMember)
                    .map(memBer -> (ClassicMember) memBer)
                    .collect(Collectors.toList())
            ) {
                final OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(members.getUniqueId());
                if (!offlinePlayer.isOnline())
                    continue;
                if (!member.equals(members))
                    Log.message(offlinePlayer.getPlayer(), Message.INFO_PLAYER_JOIN_OTHERS.get(CAdapter.adapt(player).getName(), group.getName()));
                else
                    Log.message(CAdapter.adapt(player), Message.INFO_PLAYER_JOIN_SELF.get(group.getName()));
            }
        } else
            Log.message(CAdapter.adapt(player), Message.ERROR_COMMAND_UNKNOWN);
        return !event.isCancelled();
    }

    @Override
    public List<String> tabCompleter(final CommandSender sender, final List<String> args) {
        return null;
    }
}
