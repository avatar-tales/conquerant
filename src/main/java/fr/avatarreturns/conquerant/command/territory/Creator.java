package fr.avatarreturns.conquerant.command.territory;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.logs.Log;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;
import java.util.List;

public class Creator implements SubCommand {

    @Override
    @Command(command = "territory creator", usage = "{label} {command}", executor = Command.ExecutorType.PLAYERS)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        assert sender instanceof Player;
        final ItemStack itemStack = new ItemStack(Material.DIAMOND_PICKAXE);
        final ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName("§cSELECTOR");
        meta.setLore(Collections.singletonList("  §7§l» §eCUBOID"));
        itemStack.setItemMeta(meta);
        ((Player) sender).getInventory().addItem(itemStack);
        Log.message(sender, Message.INFO_SELECTOR_RECEIVE);
        return true;
    }

    @Override
    public List<String> tabCompleter(CommandSender sender, List<String> args) {
        return null;
    }

}
