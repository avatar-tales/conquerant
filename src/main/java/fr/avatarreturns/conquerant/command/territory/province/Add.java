package fr.avatarreturns.conquerant.command.territory.province;

import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.entities.EditableEntity;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import fr.avatarreturns.conquerant.entities.territory.CLand;
import fr.avatarreturns.conquerant.entities.territory.CuboidCLand;
import fr.avatarreturns.conquerant.entities.territory.PolygonalCLand;
import fr.avatarreturns.conquerant.listeners.editor.RegionCreator;
import fr.avatarreturns.conquerant.listeners.manager.ListeningManager;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Add implements SubCommand {

    @Override
    @Command(command = "territory province add", usage = "{label} {command} <name>", arguments = 1, executor = Command.ExecutorType.PLAYERS)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        assert sender instanceof Player;
        if (!ConquerantPlugin.get().getEditingEntities().containsKey(sender))  {
            Log.message(sender, Message.ERROR_COMMAND_EXEC_EDIT_NEDITING);
            return false;
        }
        final EditableEntity entity = ConquerantPlugin.get().getEditingEntities().get(sender);
        if (!(entity instanceof CProvince)) {
            Log.message(sender, Message.ERROR_COMMAND_EXEC_EDIT_NPROVINCE);
            return false;
        }
        if (CProvince.getLand(args.get(0)).isPresent()) {
            Log.message(sender, Message.ERROR_PROVINCE_LAND_EXISTS.get(args.get(0)));
            return false;
        }
        final RegionCreator regionCreator = (RegionCreator) ListeningManager.get().getClass(RegionCreator.class);
        if (!regionCreator.getEditors().containsKey(sender)) {
            Log.message(sender, Message.ERROR_SELECTOR_NSET);
            return false;
        }
        final Pair<RegionCreator.Selector, List<CLocation>> pair = regionCreator.getEditors().get(sender);
        CLand region;
        if (pair.getFirst().equals(RegionCreator.Selector.POLYGONAL)) {
            region = new PolygonalCLand(args.get(0), pair.getSecond().get(0), pair.getSecond().get(0), pair.getSecond());
        } else if (pair.getFirst().equals(RegionCreator.Selector.CUBOID)) {
            region = new CuboidCLand(args.get(0), pair.getSecond().get(0), pair.getSecond().get(0));
        } else {
            Log.message(sender, Message.ERROR_COMMAND_UNKNOWN);
            return false;
        }
        ((CProvince) entity).getLands().add(region);
        Log.message(sender, Message.INFO_COMMAND_EXEC_PROVINCE_ADD.get(args.get(0), ((CProvince) entity).getName()));
        return true;
    }

    @Override
    public List<String> tabCompleter(CommandSender sender, List<String> args) {
        return null;
    }
}
