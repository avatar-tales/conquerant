package fr.avatarreturns.conquerant.command.territory.province;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public class Edit implements SubCommand {

    @Override
    @Command(command = "territory province edit", usage = "{label} {command} <province>", arguments = 1, executor = Command.ExecutorType.PLAYERS)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        assert sender instanceof Player;
        if (ConquerantPlugin.get().getEditingEntities().containsKey(sender))  {
            Log.message(sender, Message.ERROR_COMMAND_EXEC_EDIT_EDITING);
            return false;
        }
        final Optional<CProvince> province = CProvince.getProvince(args.get(0));

        if (province.isPresent()) {
            if (ConquerantPlugin.get().getEditingEntities().containsValue(province.get()))  {
                Log.message(sender, Message.ERROR_COMMAND_EXEC_EDIT_ENTITY);
                return false;
            }
            ConquerantPlugin.get().getEditingEntities().put((Player) sender, province.get());
            Log.message(sender, Message.INFO_COMMAND_EXEC_EDIT.get(args.get(0)));
            return true;
        }
        Log.message(sender, Message.ERROR_GROUP_NEXISTS.get(args.get(0)));
        return false;
    }

    @Override
    public List<String> tabCompleter(CommandSender sender, List<String> args) {
        return null;
    }
}
