package fr.avatarreturns.conquerant.command.territory.province;

import fr.avatarreturns.conquerant.command.manager.Command;
import fr.avatarreturns.conquerant.command.manager.SubCommand;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import org.bukkit.command.CommandSender;

import java.util.List;
import java.util.Optional;

public class Create implements SubCommand {

    @Override
    @Command(command = "territory province create", usage = "{label} {command} <name>", arguments = 1)
    public <T> boolean run(final CommandSender sender, final List<String> args, final T value) {
        final Optional<CProvince> province = CProvince.getProvince(args.get(0));
        if (province.isPresent()) {
            Log.message(sender, Message.ERROR_PROVINCE_EXISTS.get(args.get(0)));
            return false;
        }
        CProvince.getProvinces().add(new CProvince(args.get(0)));
        Log.message(sender, Message.INFO_PROVINCE_CREATED.get(args.get(0)));
        return true;
    }

    @Override
    public List<String> tabCompleter(CommandSender sender, List<String> args) {
        return null;
    }
}
