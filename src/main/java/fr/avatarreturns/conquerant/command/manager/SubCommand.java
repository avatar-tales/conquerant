package fr.avatarreturns.conquerant.command.manager;

import org.bukkit.command.CommandSender;

import java.util.List;

public interface SubCommand {

    <T> boolean run(final CommandSender sender, final List<String> args, final T value);

    List<String> tabCompleter(final CommandSender sender, final List<String> args);

}
