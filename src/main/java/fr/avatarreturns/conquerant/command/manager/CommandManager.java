package fr.avatarreturns.conquerant.command.manager;

import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandManager implements CommandExecutor, TabCompleter {

    private static CommandManager instance;

    public static CommandManager get() {
        if (instance == null)
            instance = new CommandManager();
        return instance;
    }

    private final List<SubCommand> subCommands;

    private CommandManager() {
        this.subCommands = new ArrayList<>();
        try {
            for (final Class<?> clazz : ConquerantPlugin.get().loadClasses("fr.avatarreturns.conquerant.command", "groups", "territory")) {
                if (SubCommand.class.isAssignableFrom(clazz))
                    this.subCommands.add((SubCommand) clazz.newInstance());
            }
        } catch (IOException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCommand(final @NotNull CommandSender sender, final @NotNull org.bukkit.command.Command command, final @NotNull String label, @NotNull String[] args) {

        if (args.length == 0)
            args = new String[] { "help" };

        Pair<Pair<SubCommand, Method>, Pair<Command, String>> toExec = null;

        final StringBuilder builder = new StringBuilder();
        final StringBuilder rawArgs = new StringBuilder();
        Pair<Boolean, String> concatenateArgs = Pair.from(false, "");
        for (final String arg : args) {
            rawArgs.append(" ").append(arg);
            if (concatenateArgs.getFirst()) {
                if (arg.endsWith("\"") && !arg.endsWith("\\\"") && concatenateArgs.getSecond().equalsIgnoreCase("\"")) {
                    concatenateArgs = Pair.from(false, "");
                    builder.append(" ").append(arg, 0, arg.length() - 1).append(",");
                } else if (arg.endsWith("'") && !arg.endsWith("\\'") && concatenateArgs.getSecond().equalsIgnoreCase("'")) {
                    concatenateArgs = Pair.from(false, "");
                    builder.append(" ").append(arg, 0, arg.length() - 1).append(",");
                } else
                    builder.append(" ").append(arg);
            } else {
                if (arg.startsWith("\"") && !arg.endsWith("\"") && !arg.startsWith("\\\"")) {
                    concatenateArgs = Pair.from(true, "\"");
                    builder.append(arg.substring(1));
                } else if (arg.startsWith("'") && !arg.endsWith("'") && !arg.startsWith("\\'")) {
                    concatenateArgs = Pair.from(true, "'");
                    builder.append(arg.substring(1));
                } else
                    builder.append(arg).append(",");

            }
        }
        rawArgs.deleteCharAt(0);
        String argument = (builder.charAt(builder.length() - 1) == ',' ?
                builder.deleteCharAt(builder.length() - 1).toString() :
                builder.toString()).replace("\\\"", "\"").replace("\\'", "'");

        for (final SubCommand subCommand : this.subCommands) {
            for (final Method method : subCommand.getClass().getDeclaredMethods()) {
                final Command annotation = method.getAnnotation(Command.class);
                if (annotation == null)
                    continue;

                for (final String aliases : annotation.command()) {

                    if (!argument.toLowerCase().startsWith(aliases.replace(" ", ",").toLowerCase()))
                        continue;

                    if (toExec == null)
                        toExec = Pair.from(Pair.from(subCommand, method), Pair.from(annotation, aliases));
                    else if (toExec.getSecond().getSecond().length() < aliases.length())
                        toExec = Pair.from(Pair.from(subCommand, method), Pair.from(annotation, aliases));
                }
            }
        }

        if (toExec != null) {

            if (toExec.getSecond().getFirst().executor() != Command.ExecutorType.ALL) {
                if (toExec.getSecond().getFirst().executor() == Command.ExecutorType.CONSOLE && sender instanceof Player) {
                    Log.message(sender, Message.ERROR_COMMAND_EXECUTOR_CONSOLE.get(this.reformatUsage(label, toExec.getSecond().getSecond(), toExec.getSecond().getFirst())));
                    return false;
                } else if (toExec.getSecond().getFirst().executor() == Command.ExecutorType.PLAYERS && !(sender instanceof Player)) {
                    Log.message(sender, Message.ERROR_COMMAND_EXECUTOR_PLAYER.get(this.reformatUsage(label, toExec.getSecond().getSecond(), toExec.getSecond().getFirst())));
                    return false;
                }
            }

            final String finalArg = argument.replace(toExec.getSecond().getSecond().replace(" ", ","), "")
                    .replaceFirst("[,]", "");

            final List<String> arguments = (finalArg.equals("") ?
                    new ArrayList<>() :
                    new ArrayList<>(Arrays.asList(finalArg.split(",")))
            );

            if (arguments.size() >= toExec.getSecond().getFirst().arguments()) {
                try {
                    return (boolean) toExec.getFirst().getSecond().invoke(toExec.getFirst().getFirst(), sender, arguments, null);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            } else
                Log.message(sender, Message.ERROR_COMMAND_ARGUMENTS.get(this.reformatUsage(label, toExec.getSecond().getSecond(), toExec.getSecond().getFirst()), String.valueOf(toExec.getSecond().getFirst().arguments() - arguments.size())));
        } else {
            Log.message(sender, Message.ERROR_COMMAND_NEXISTS.get(label + " " + rawArgs.toString()));
        }
        return false;
    }

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull org.bukkit.command.Command command, @NotNull String alias, @NotNull String[] args) {
        final List<String> output = new ArrayList<>();
        this.subCommands.forEach(subCommand -> {
            final List<String> result = subCommand.tabCompleter(sender, Arrays.asList(args));
            if (result != null)
                output.addAll(result);
        });
        return output;
    }

    private String reformatUsage(final Command annotation) {
        return this.reformatUsage("conquerant", annotation.command()[0], annotation);
    }

    private String reformatUsage(final String label, final Command annotation) {
        return this.reformatUsage(label, annotation.command()[0], annotation);
    }

    private String reformatUsage(final String label, final String aliases, final Command annotation) {
        return annotation.usage().replace("{label}", label).replace("{command}", aliases);
    }

}
