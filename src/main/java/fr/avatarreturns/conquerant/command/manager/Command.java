package fr.avatarreturns.conquerant.command.manager;

import fr.avatarreturns.conquerant.config.Permission;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Command {

    String[] command();

    String usage() default "{label} help";

    int arguments() default 0;

    Permission permission() default Permission.NONE;

    ExecutorType executor() default ExecutorType.ALL;

    enum ExecutorType {
        ALL,
        PLAYERS,
        CONSOLE;
    }

}
