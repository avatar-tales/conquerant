package fr.avatarreturns.conquerant.entities.adapted.objects;

import org.bukkit.Bukkit;

import java.util.UUID;

public class CLocation {

    private final UUID worldId;
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    public CLocation(final UUID worldId, final int x, final int y, final int z) {
        this(worldId, x + 0D, y + 0D, z + 0D);
    }

    public CLocation(final UUID worldId, final double x, final double y, final double z) {
        this(worldId, x, y, z, 0, 0);
    }

    public CLocation(final UUID worldId, final double x, final double y, final double z, final float yaw, final float pitch) {
        this.worldId = worldId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public void add(final double x, final double z, final double y) {
        this.add(x, y, z, 0, 0);
    }

    public void add(final double x, final double z, final double y, final float yaw, final float pitch) {
        this.x += x;
        this.z += z;
        this.y += z;
        this.yaw += yaw;
        this.pitch += pitch;
    }

    public void subtract(final double x, final double z, final double y) {
        this.subtract(x, y, z, 0, 0);
    }

    public void subtract(final double x, final double z, final double y, final float yaw, final float pitch) {
        this.x -= x;
        this.z -= z;
        this.y -= z;
        this.yaw -= yaw;
        this.pitch -= pitch;
    }

    public void set(final double x, final double y, final double z) {
        this.set(x, y, z, this.yaw, this.pitch);
    }

    public void set(final double x, final double y, final double z, final float yaw, final float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public UUID getWorldRawId() {
        return this.worldId;
    }

    public String getWorldId() {
        return this.worldId.toString();
    }

    public double getX() {
        return this.x;
    }

    public int getBlockX() {
        return (int) Math.floor(this.x);
    }

    public int getChunkX() {
        return (int) Math.ceil((this.x - 16) / 16);
    }

    public void setX(final double x) {
        this.x = x;
    }

    public double getY() {
        return this.y;
    }

    public int getBlockY() {
        return (int) Math.floor(this.y);
    }

    public int getChunkY() {
        return (int) Math.ceil((this.y - 16) / 16);
    }

    public void setY(final double y) {
        this.y = y;
    }

    public double getZ() {
        return this.z;
    }

    public int getBlockZ() {
        return (int) Math.floor(this.z);
    }

    public int getChunkZ() {
        return (int) Math.ceil((this.z - 16) / 16);
    }

    public void setZ(final double z) {
        this.z = z;
    }

    public float getYaw() {
        return this.yaw;
    }

    public void setYaw(final float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return this.pitch;
    }

    public void setPitch(final float pitch) {
        this.pitch = pitch;
    }

    public CLocation clone() {
        return new CLocation(this.worldId, this.x, this.y, this.z, this.yaw, this.pitch);
    }

    public String toString() {
        return "(" + Bukkit.getWorld(this.worldId).getName() + ", " + this.x + ", " + this.y + ", " + this.z + ", " + this.yaw + ", " + this.pitch + ")";
    }

    public String toStringBlock() {
        return "(" + Bukkit.getWorld(this.worldId).getName() + ", " + this.getBlockX() + ", " + this.getBlockY() + ", " + this.getBlockZ() + ")";
    }

    public String toStringBlockWithoutWorld() {
        return "(" + this.getBlockX() + ", " + this.getBlockY() + ", " + this.getBlockZ() + ")";
    }
}
