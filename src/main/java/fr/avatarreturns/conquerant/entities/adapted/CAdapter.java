package fr.avatarreturns.conquerant.entities.adapted;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class CAdapter {

    public static CLocation adapt(final Location location) {
        return new CLocation(location.getWorld().getUID(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public static Location adapt(final CLocation location) {
        return new Location(Bukkit.getWorld(location.getWorldRawId()), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public static Player adapt(final CPlayer player) {
        return Bukkit.getPlayer(player.getUniqueId());
    }

    public static CPlayer adapt(final Player player) {
        return CPlayer.getPlayer(player.getUniqueId()).orElseGet(() -> {
            final CPlayer cplayer = new CPlayer(player.getUniqueId());
            CPlayer.getPlayers().add(cplayer);
            return cplayer;
        });
    }
}
