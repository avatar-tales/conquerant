package fr.avatarreturns.conquerant.entities.adapted.objects;

import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.utils.Pair;

import java.util.*;

public class CPlayer {

    private static final List<CPlayer> players = new ArrayList<>();

    public static List<CPlayer> getPlayers() {
        return players;
    }

    public static Optional<CPlayer> getPlayer(final UUID uniqueId) {
        return players.stream().filter(player -> player.getUniqueId().equals(uniqueId)).findFirst();
    }

    private final UUID uniqueId;

    private int power;
    private boolean override;

    private final LinkedHashMap<Group, Pair<Optional<UUID>, Long>> pendingInvitations;

    private transient UUID actualLand;

    public CPlayer(final UUID uuid) {
        this.uniqueId = uuid;
        this.override = false;
        this.pendingInvitations = new LinkedHashMap<>();
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public boolean isOverride() {
        return this.override;
    }

    public void setOverride(final boolean override) {
        this.override = override;
    }

    public LinkedHashMap<Group, Pair<Optional<UUID>, Long>> getPendingInvitations() {
        return this.pendingInvitations;
    }

    public UUID getActualLand() {
        return this.actualLand;
    }

    public void setActualLand(final UUID actualLand) {
        this.actualLand = actualLand;
    }
}
