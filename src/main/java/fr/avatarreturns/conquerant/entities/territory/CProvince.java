package fr.avatarreturns.conquerant.entities.territory;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.entities.EditableEntity;
import fr.avatarreturns.conquerant.utils.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class CProvince implements EditableEntity {

    private static final List<CProvince> provinces = new ArrayList<>();

    public static List<CProvince> getProvinces() {
        return provinces;
    }

    public static Optional<CProvince> getProvince(final UUID uniqueId) {
        return provinces.stream().filter(cProvince -> cProvince.getUniqueId().equals(uniqueId)).findFirst();
    }

    public static Optional<CProvince> getProvince(final String nameOrUUID) {
        return provinces.stream().filter(cProvince -> cProvince.getName().equalsIgnoreCase(nameOrUUID) || cProvince.getUniqueId().toString().equalsIgnoreCase(nameOrUUID)).findFirst();
    }

    public static Optional<Pair<CProvince, CLand>> getLand(final UUID uniqueId) {
        Optional<Pair<CProvince, CLand>> output = Optional.empty();
        for (final CProvince province : provinces) {
            if (output.isPresent())
                break;
            for (final CLand land : province.getLands()) {
                if (land.getUniqueId().equals(uniqueId)) {
                    output = Optional.of(Pair.from(province, land));
                    break;
                }
            }
        }
        return output;
    }

    public static Optional<Pair<CProvince, CLand>> getLand(final String name) {
        Optional<Pair<CProvince, CLand>> output = Optional.empty();
        for (final CProvince province : provinces) {
            if (output.isPresent())
                break;
            for (final CLand land : province.getLands()) {
                if (land.getUniqueId().toString().equalsIgnoreCase(name) || land.getName().equalsIgnoreCase(name)) {
                    output = Optional.of(Pair.from(province, land));
                    break;
                }
            }
        }
        return output;
    }

    public static Optional<Pair<CProvince, CLand>> getLand(final CLocation location) {
        Optional<Pair<CProvince, CLand>> output = Optional.empty();
        for (final CProvince province : provinces) {
            if (output.isPresent())
                break;
            for (final CLand land : province.getLands()) {
                if (land.isInside(location)) {
                    output = Optional.of(Pair.from(province, land));
                    break;
                }
            }
        }
        return output;
    }

    private final UUID uniqueId;
    private String name;
    private final List<CLand> lands;

    public CProvince(final String name) {
        this.uniqueId = UUID.randomUUID();
        this.name = name;
        this.lands = new ArrayList<>();
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<CLand> getLands() {
        return this.lands;
    }
}
