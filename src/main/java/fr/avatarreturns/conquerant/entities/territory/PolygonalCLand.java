package fr.avatarreturns.conquerant.entities.territory;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;

import java.util.ArrayList;
import java.util.List;

public class PolygonalCLand extends CLand {

    private final List<CLocation> points;

    public PolygonalCLand(final String name, final CLocation maxLocation, final CLocation minLocation) {
        this(name, maxLocation, minLocation, new ArrayList<>());
    }

    public PolygonalCLand(final String name, final CLocation firstPoint, final CLocation lastPoint, final List<CLocation> points) {
        super(name, firstPoint, lastPoint);
        this.points = points;
        for (final CLocation point : this.points) {
            point.setY(0);
            if (this.getMinLocation().getX() > point.getX() && this.getMinLocation().getZ() > point.getZ())
                this.setMinLocation(point);
            else if (this.getMaxLocation().getX() < point.getX() && this.getMaxLocation().getZ() < point.getZ())
                this.setMaxLocation(point);
        }
    }

    @Override
    public boolean isInside(final CLocation location) {
        if (this.points.size() < 3) {
            return false;
        }
        int targetX = location.getBlockX();
        int targetZ = location.getBlockZ();

        boolean inside = false;
        int npoints = this.points.size();
        int xNew;
        int zNew;
        int x1;
        int z1;
        int x2;
        int z2;
        long crossproduct;
        int i;

        int xOld = points.get(npoints - 1).getBlockX();
        int zOld = points.get(npoints - 1).getBlockZ();

        for (i = 0; i < npoints; ++i) {
            xNew = this.points.get(i).getBlockX();
            zNew = this.points.get(i).getBlockZ();
            //Check for corner
            if (xNew == targetX && zNew == targetZ) {
                return true;
            }
            if (xNew > xOld) {
                x1 = xOld;
                x2 = xNew;
                z1 = zOld;
                z2 = zNew;
            } else {
                x1 = xNew;
                x2 = xOld;
                z1 = zNew;
                z2 = zOld;
            }
            if (x1 <= targetX && targetX <= x2) {
                crossproduct = ((long) targetZ - (long) z1) * (long) (x2 - x1)
                        - ((long) z2 - (long) z1) * (long) (targetX - x1);
                if (crossproduct == 0) {
                    if ((z1 <= targetZ) == (targetZ <= z2)) {
                        return true; //on edge
                    }
                } else if (crossproduct < 0 && (x1 != targetX)) {
                    inside = !inside;
                }
            }
            xOld = xNew;
            zOld = zNew;
        }

        return inside;
    }

    public List<CLocation> getPoints() {
        return this.points;
    }

    @Override
    public RegionType getType() {
        return RegionType.POLYGONAL;
    }
}
