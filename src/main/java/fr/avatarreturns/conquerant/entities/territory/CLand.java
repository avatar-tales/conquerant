package fr.avatarreturns.conquerant.entities.territory;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;

import java.util.UUID;

public abstract class CLand {

    private final UUID uniqueId;
    private String name;

    private CLocation maxLocation;
    private CLocation minLocation;

    private RegionStatus status;

    public CLand(final String name, final CLocation firstPoint, final CLocation lastPoint) {
        this.uniqueId = UUID.randomUUID();
        this.name = name;
        double maxX, minX;
        double maxZ, minZ;
        if (firstPoint.getX() > lastPoint.getX()) {
            maxX = firstPoint.getX();
            minX = lastPoint.getX();
        } else {
            maxX = lastPoint.getX();
            minX = firstPoint.getX();
        } if (firstPoint.getZ() > lastPoint.getZ()) {
            maxZ = firstPoint.getZ();
            minZ = lastPoint.getZ();
        } else {
            maxZ = lastPoint.getZ();
            minZ = firstPoint.getZ();
        }
        this.maxLocation = new CLocation(firstPoint.getWorldRawId(), maxX, 0, maxZ);
        this.minLocation = new CLocation(firstPoint.getWorldRawId(), minX, 0, minZ);
        this.status = RegionStatus.FREE;
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public CLocation getMaxLocation() {
        return this.maxLocation;
    }

    protected void setMaxLocation(final CLocation location) {
        this.maxLocation = location;
    }

    public CLocation getMinLocation() {
        return this.minLocation;
    }

    protected void setMinLocation(final CLocation location) {
        this.minLocation = location;
    }

    public abstract boolean isInside(final CLocation location);

    public abstract RegionType getType();

    public RegionStatus getStatus() {
        return this.status;
    }

    public void setStatus(final RegionStatus status) {
        this.status = status;
    }

    public enum RegionType {
        CUBOID,
        POLYGONAL
    }

    public enum RegionStatus {
        FREE,
        OCCUPIED;
    }
}
