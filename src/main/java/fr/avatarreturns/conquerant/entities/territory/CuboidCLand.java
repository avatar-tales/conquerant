package fr.avatarreturns.conquerant.entities.territory;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;

public class CuboidCLand extends CLand {

    public CuboidCLand(final String name, final CLocation maxLocation, final CLocation minLocation) {
        super(name, maxLocation, minLocation);
    }

    @Override
    public boolean isInside(final CLocation location) {
        boolean inside = false;
        if (location.getBlockX() >= this.getMinLocation().getBlockX() && location.getBlockX() <= this.getMaxLocation().getBlockX())
            if (location.getBlockZ() >= this.getMinLocation().getBlockZ() && location.getBlockZ() <= this.getMaxLocation().getBlockZ())
                inside = true;
        return inside;
    }

    @Override
    public RegionType getType() {
        return RegionType.CUBOID;
    }
}
