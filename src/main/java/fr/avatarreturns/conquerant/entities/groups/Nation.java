package fr.avatarreturns.conquerant.entities.groups;

public class Nation extends Group {

    public Nation(final String name) {
        super(name);
    }

    @Override
    public GroupType getType() {
        return GroupType.NATION;
    }

}
