package fr.avatarreturns.conquerant.entities.groups.members;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.utils.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class Member {

    private final UUID uniqueId;

    public final Map<Pair<Integer, Integer>, Boolean> buildPermissions;
    public final Map<Pair<Integer, Integer>, Boolean> interactPermissions;

    public Member(final UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.buildPermissions = new HashMap<>();
        this.interactPermissions = new HashMap<>();
    }

    public abstract boolean canBuild(final CLocation location);

    public abstract boolean canInteract(final CLocation location);

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public abstract MemberType getType();

    public enum MemberType {
        CLASSIC,
        PERMISSION
    }
}
