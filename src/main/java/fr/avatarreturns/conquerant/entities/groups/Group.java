package fr.avatarreturns.conquerant.entities.groups;

import fr.avatarreturns.conquerant.entities.EditableEntity;
import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.entities.groups.members.Member;
import fr.avatarreturns.conquerant.entities.territory.CLand;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import fr.avatarreturns.conquerant.utils.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class Group implements EditableEntity {

    private static transient final List<Group> registered = new ArrayList<>();

    public static List<Group> getGroups() {
        return registered;
    }

    public static @NotNull Optional<Group> getGroupById(final UUID id) {
        for (final Group group : registered) {
            if (group.getUniqueId().equals(id))
                return Optional.of(group);
        }
        return Optional.empty();
    }

    public static @NotNull List<Group> getGroupsWithPlayer(final CPlayer player) {
        final List<Group> result = new ArrayList<>();
        for (final Group group : registered) {
            group.getMembers().stream()
                    .filter(member -> member.getUniqueId().equals(player.getUniqueId()))
                    .findFirst()
                    .ifPresent(member -> result.add(group));
        }
        return result;
    }

    public static @NotNull Optional<Group> getGroupByName(final String name) {
        return registered.stream().filter(group -> group.getName().equalsIgnoreCase(name) || group.getUniqueId().toString().equalsIgnoreCase(name)).findFirst();
    }

    public static @NotNull Optional<Pair<Group, Pair<CProvince, CLand>>> getGroupByLand(final String name) {
        return getGroupByCouple(CProvince.getLand(name));
    }

    public static @NotNull Optional<Pair<Group, Pair<CProvince, CLand>>> getGroupByLand(final CLocation location) {
        return getGroupByCouple(CProvince.getLand(location));
    }

    private static @NotNull Optional<Pair<Group, Pair<CProvince, CLand>>> getGroupByCouple(Optional<Pair<CProvince, CLand>> land) {
        Optional<Pair<Group, Pair<CProvince, CLand>>> output = Optional.empty();
        if (land.isPresent()) {
            if (land.get().getSecond().getStatus() == CLand.RegionStatus.FREE)
                return output;
            final UUID landId = land.get().getSecond().getUniqueId();
            group:
            for (final Group group : registered) {
                for (final UUID landsId : group.getRegions()) {
                    if (landId.equals(landsId)) {
                        output = Optional.of(Pair.from(group, land.get()));
                        break group;
                    }
                }
            }
        }
        return output;
    }

    public static @NotNull Optional<Pair<Group, CLand>> getGroupByLand(final CLand cLand) {
        Optional<Pair<Group, CLand>> output = Optional.empty();
        group:
        for (final Group group : registered) {
            for (final UUID landsId : group.getRegions()) {
                if (cLand.getUniqueId().equals(landsId)) {
                    output = Optional.of(Pair.from(group, cLand));
                    break group;
                }
            }
        }
        return output;
    }

    public static Optional<Pair<Group, ClassicMember>> getGroupByPlayer(final CPlayer player) {
        for (final Group group : registered) {
            final Optional<ClassicMember> member = group.getMemberWithId(player.getUniqueId());
            if (member.isPresent())
                return Optional.of(Pair.from(group, member.get()));
        }
        return Optional.empty();
    }

    private final UUID uniqueId;
    private String name;
    private double money;

    private boolean open;

    private final List<Member> members;
    private final List<UUID> regions;

    public Group(final String name) {
        this.uniqueId = UUID.randomUUID();
        this.name = name;
        this.members = new ArrayList<>();
        this.regions = new ArrayList<>();
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setMoney(final double money) {
        this.money = money;
    }

    public double getMoney() {
        return this.money;
    }

    public void setOpen(final boolean open) {
        this.open = open;
    }

    public boolean isOpen() {
        return open;
    }

    public List<Member> getMembers() {
        return this.members;
    }

    public List<ClassicMember> getClassicMembers() {
        return this.members.stream().filter(member -> member instanceof ClassicMember)
                .map(member -> (ClassicMember) member)
                .collect(Collectors.toList());
    }

    public List<UUID> getRegions() {
        return this.regions;
    }

    public abstract GroupType getType();

    public boolean containsMemberWithId(final UUID uniqueId) {
        for (final Member member : this.members) {
            if (member.getUniqueId().equals(uniqueId))
                return true;
        }
        return false;
    }

    public Optional<Member> getAllMemberWithId(final UUID uniqueId) {
        for (final Member member : this.members) {
            if (member.getUniqueId().equals(uniqueId)) {
                return Optional.of(member);
            }
        }
        return Optional.empty();
    }

    public Optional<ClassicMember> getMemberWithId(final UUID uniqueId) {
        for (final Member member : this.members) {
            if (member.getUniqueId().equals(uniqueId) && member instanceof ClassicMember) {
                return Optional.of((ClassicMember) member);
            }
        }
        return Optional.empty();
    }

    public void setLeader(final ClassicMember member) {
        for (final ClassicMember members : this.members.stream()
                .filter(members -> members instanceof ClassicMember)
                .map(members -> (ClassicMember) members)
                .collect(Collectors.toList())
        ) {
            if (members.getRank() == Ranks.LEADER)
                members.setRank(Ranks.OFFICIER);
        }
        member.setRank(Ranks.LEADER);
    }

    public enum GroupType {

        NATION("");

        private final String value;

        GroupType(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static GroupType getTypeByValue(final String value) {
            for (final GroupType type : GroupType.values()) {
                if (type.getValue().equalsIgnoreCase(value))
                    return type;
            }
            return GroupType.NATION;
        }
    }

    public enum Ranks {
        LEADER(4, true, true, true, true, true),
        OFFICIER(3, true, true, true, true, false),
        MEMBER(2, true, true, false, false, false),
        RECRUIT(1, true, false, false, false, false);

        private final int power;

        boolean build;
        boolean interact;
        boolean claim;
        boolean invite;
        boolean kick;

        Ranks(final int power, final boolean build, final boolean interact, final boolean claim, final boolean invite, final boolean kick) {
            this.power = power;
            this.build = build;
            this.interact = interact;
            this.claim = claim;
            this.invite = invite;
            this.kick = kick;
        }

        public int getPower() {
            return this.power;
        }

        public boolean canBuild() {
            return this.build;
        }

        public void setBuild(final boolean build) {
            this.build = build;
        }

        public boolean canInteract() {
            return this.interact;
        }

        public void setInteract(final boolean interact) {
            this.interact = interact;
        }

        public boolean canClaim() {
            return this.claim;
        }

        public void setClaim(final boolean claim) {
            this.claim = claim;
        }

        public boolean canInvite() {
            return this.invite;
        }

        public void setInvite(final boolean invite) {
            this.invite = invite;
        }

        public boolean canKick() {
            return this.kick;
        }

        public void setKick(final boolean kick) {
            this.kick = kick;
        }
    }
}
