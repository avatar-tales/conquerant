package fr.avatarreturns.conquerant.entities.groups.members;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.utils.Pair;

import java.util.Map;
import java.util.UUID;

public class PermissionMember extends Member {

    public PermissionMember(final UUID uniqueId) {
        super(uniqueId);
    }

    @Override
    public boolean canBuild(final CLocation location) {
        return result(location, this.buildPermissions);
    }

    @Override
    public boolean canInteract(final CLocation location) {
        return result(location, this.interactPermissions);
    }

    private Boolean result(final CLocation location, final Map<Pair<Integer, Integer>, Boolean> map) {
        final Pair<Integer, Integer> chunkPosition = Pair.from(location.getChunkX(), location.getChunkZ());
        for (final Map.Entry<Pair<Integer, Integer>, Boolean> entry : map.entrySet()) {
            if (entry.getKey().equals(chunkPosition))
                continue;
            return entry.getValue();
        }
        return false;
    }

    @Override
    public MemberType getType() {
        return MemberType.PERMISSION;
    }
}
