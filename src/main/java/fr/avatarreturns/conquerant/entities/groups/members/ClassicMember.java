package fr.avatarreturns.conquerant.entities.groups.members;

import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.utils.Pair;

import java.util.UUID;

public class ClassicMember extends Member {

    private Group.Ranks rank;

    public ClassicMember(final UUID uniqueId) {
        super(uniqueId);
        this.rank = Group.Ranks.RECRUIT;
    }

    @Override
    public boolean canBuild(final CLocation location) {
        final Pair<Integer, Integer> chunkPosition = Pair.from(location.getChunkX(), location.getChunkZ());
        if (this.buildPermissions.containsKey(chunkPosition))
            return this.buildPermissions.get(chunkPosition);
        return this.rank.canBuild();
    }

    @Override
    public boolean canInteract(final CLocation location) {
        final Pair<Integer, Integer> chunkPosition = Pair.from(location.getChunkX(), location.getChunkZ());
        if (this.interactPermissions.containsKey(chunkPosition))
            return this.interactPermissions.get(chunkPosition);
        return this.rank.canInteract();
    }

    @Override
    public MemberType getType() {
        return MemberType.CLASSIC;
    }

    public Group.Ranks getRank() {
        return this.rank;
    }

    public void setRank(final Group.Ranks rank) {
        this.rank = rank;
    }
}
