package fr.avatarreturns.conquerant.events.group;

import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class GroupCreateEvent extends Event implements Cancellable {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final Optional<CPlayer> leader;
    private final Group group;

    private boolean cancelled;

    public GroupCreateEvent(final Optional<CPlayer> leader, final Group group) {
        this.leader = leader;
        this.group = group;
        this.cancelled = false;
    }

    public Optional<CPlayer> getLeader() {
        return this.leader;
    }

    public Group getGroup() {
        return this.group;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(final boolean cancel) {
        this.cancelled = cancel;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
