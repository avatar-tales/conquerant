package fr.avatarreturns.conquerant.events.group;

import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.territory.CLand;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class GroupClaimEvent extends Event implements Cancellable {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final CPlayer player;
    private final Group group;
    private final CProvince province;
    private final CLand land;

    private boolean cancelled;

    public GroupClaimEvent(final CPlayer player, final Group group, final CProvince province, final CLand land) {
        this.player = player;
        this.group = group;
        this.province = province;
        this.land = land;
        this.cancelled = false;
    }

    public CPlayer getPlayer() {
        return this.player;
    }

    public Group getGroup() {
        return this.group;
    }

    public CProvince getProvince() {
        return this.province;
    }

    public CLand getLand() {
        return this.land;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(final boolean cancel) {
        this.cancelled = cancel;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
