package fr.avatarreturns.conquerant.events.group;

import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.events.player.PlayerQuitEvent;
import fr.avatarreturns.conquerant.utils.Pair;

import java.util.Optional;
import java.util.UUID;

public class GroupKickEvent extends PlayerQuitEvent {

    private final boolean byOperator;
    private final Optional<UUID> executor;

    public GroupKickEvent(final Pair<CPlayer, ClassicMember> player, final Group group, final boolean byOperator, final Optional<UUID> executor) {
        super(player, group);
        this.byOperator = byOperator;
        this.executor = executor;
    }

    public boolean isByOperator() {
        return this.byOperator;
    }

    public Optional<UUID> getExecutor() {
        return this.executor;
    }
}
