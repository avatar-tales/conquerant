package fr.avatarreturns.conquerant.events.player;

import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.members.ClassicMember;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class PlayerQuitEvent extends Event implements Cancellable {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final Pair<CPlayer, ClassicMember> player;
    private final Group group;

    private boolean cancelled;

    public PlayerQuitEvent(final Pair<CPlayer, ClassicMember> player, final Group group) {
        this.player = player;
        this.group = group;
        this.cancelled = false;
    }

    public Pair<CPlayer, ClassicMember> getPlayer() {
        return this.player;
    }

    public Group getGroup() {
        return this.group;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(final boolean cancel) {
        this.cancelled = cancel;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
