package fr.avatarreturns.conquerant.events.player;

import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.territory.CLand;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PlayerChangeLandEvent extends Event implements Cancellable {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final CPlayer player;
    private final Pair<CProvince, CLand> formerLocation;
    private final Pair<CProvince, CLand> newLocation;

    private boolean cancelled;

    public PlayerChangeLandEvent(final CPlayer player, final Pair<CProvince, CLand> formerLocation, final Pair<CProvince, CLand> newLocation) {
        this.player = player;
        this.formerLocation = formerLocation;
        this.newLocation = newLocation;
        this.cancelled = false;
    }

    public CPlayer getPlayer() {
        return this.player;
    }

    @Nullable
    public Pair<CProvince, CLand> getFormerLocation() {
        return this.formerLocation;
    }

    @Nullable
    public Pair<CProvince, CLand> getNewLocation() {
        return this.newLocation;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(final boolean cancel) {
        this.cancelled = cancel;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
