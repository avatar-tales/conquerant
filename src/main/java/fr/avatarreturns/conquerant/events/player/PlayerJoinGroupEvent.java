package fr.avatarreturns.conquerant.events.player;

import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class PlayerJoinGroupEvent extends Event implements Cancellable {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final CPlayer player;
    private final Group group;
    private final boolean byOperator;
    private final boolean withInvitation;

    private boolean cancelled;

    public PlayerJoinGroupEvent(final CPlayer player, final Group group, final boolean byOperator, final boolean withInvitation) {
        this.player = player;
        this.group = group;
        this.byOperator = byOperator;
        this.withInvitation = withInvitation;
        this.cancelled = false;
    }

    public CPlayer getPlayer() {
        return this.player;
    }

    public Group getGroup() {
        return this.group;
    }

    public boolean byOperator() {
        return this.byOperator;
    }

    public boolean withInvitation() {
        return this.withInvitation;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(final boolean cancel) {
        this.cancelled = cancel;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
