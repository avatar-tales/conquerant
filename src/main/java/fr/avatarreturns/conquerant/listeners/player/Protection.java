package fr.avatarreturns.conquerant.listeners.player;

import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.groups.members.Member;
import fr.avatarreturns.conquerant.entities.territory.CLand;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import fr.avatarreturns.conquerant.listeners.manager.SubListener;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Optional;

public class Protection implements SubListener {

    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent e) {
        e.setCancelled(cant(CAdapter.adapt(e.getPlayer()), CAdapter.adapt(e.getBlock().getLocation()), true));
    }

    @EventHandler
    public void onBlockBreak(final BlockBreakEvent e) {
        e.setCancelled(cant(CAdapter.adapt(e.getPlayer()), CAdapter.adapt(e.getBlock().getLocation()), true));
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK)
            return;
        Location location = null;
        if (e.getClickedBlock() != null)
            location = e.getClickedBlock().getLocation();
        if (location == null)
            return;
        e.setCancelled(cant(CAdapter.adapt(e.getPlayer()), CAdapter.adapt(location), false));
    }

    private boolean cant(final CPlayer player, final CLocation location, final boolean build) {
        if (player.isOverride())
            return false;
        final Optional<Pair<Group, Pair<CProvince, CLand>>> result = Group.getGroupByLand(location);
        if (result.isEmpty())
            return false;
        final Optional<Member> member = result.get().getFirst().getAllMemberWithId(player.getUniqueId());
        if (member.isPresent()) {
            if (build)
                return !member.get().canBuild(location);
            else
                return !member.get().canInteract(location);
        }
        return true;
    }

}
