package fr.avatarreturns.conquerant.listeners.player;

import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.groups.Group;
import fr.avatarreturns.conquerant.entities.territory.CLand;
import fr.avatarreturns.conquerant.entities.territory.CProvince;
import fr.avatarreturns.conquerant.events.player.PlayerChangeLandEvent;
import fr.avatarreturns.conquerant.listeners.manager.SubListener;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class Tools implements SubListener {

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent e) {
        final CPlayer player = CAdapter.adapt(e.getPlayer());
        final UUID land = player.getActualLand();
        final Optional<Pair<CProvince, CLand>> provinceLand = CProvince.getLand(land);
        final Optional<Pair<CProvince, CLand>> newProvinceLand = CProvince.getLand(CAdapter.adapt(e.getTo()));
        if (!Objects.equals(provinceLand, newProvinceLand)) {
            final PlayerChangeLandEvent event = new PlayerChangeLandEvent(player, provinceLand.orElse(null), newProvinceLand.orElse(null));
            ConquerantPlugin.get().getServer().getPluginManager().callEvent(event);
            if (event.isCancelled()) {
                e.setCancelled(true);
                return;
            }
            if (newProvinceLand.isPresent()) {
                player.setActualLand(newProvinceLand.get().getSecond().getUniqueId());
                e.getPlayer().sendTitle("§7§l- §c" + newProvinceLand.get().getFirst().getName() + "§7§l-", "§6" + newProvinceLand.get().getSecond().getName(), 20, 3 * 20, 30);
                final Optional<Pair<Group, CLand>> group = Group.getGroupByLand(newProvinceLand.get().getSecond());
                group.ifPresent(groupCLandPair -> e.getPlayer().sendTitle("§7§l- §c" + newProvinceLand.get().getFirst().getName() + "§7§l-", "§6" + newProvinceLand.get().getSecond().getName() + " §e(" + groupCLandPair.getFirst().getName() + ")", 20, 3 * 20, 30));
            } else {
                player.setActualLand(null);
                e.getPlayer().sendTitle("§7§l- §cWilderness §7§l-", "", 20, 3 * 20, 30);
            }
        }
    }
}
