package fr.avatarreturns.conquerant.listeners.player;

import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.listeners.manager.SubListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class Connexion implements SubListener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {
        CPlayer.getPlayers().add(CAdapter.adapt(e.getPlayer()));
    }
}
