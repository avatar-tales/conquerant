package fr.avatarreturns.conquerant.listeners.manager;

import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListeningManager {

    private static ListeningManager instance;

    public static ListeningManager get() {
        if (instance == null)
            instance = new ListeningManager();
        return instance;
    }

    private final List<SubListener> subListeners;

    private ListeningManager() {
        this.subListeners = new ArrayList<>();
        try {
            for (final Class<?> clazz : ConquerantPlugin.get().loadClasses("fr.avatarreturns.conquerant.listeners", "editor", "player")) {
                if (SubListener.class.isAssignableFrom(clazz)) {
                    final SubListener subListener = (SubListener) clazz.newInstance();
                    ConquerantPlugin.get().getServer().getPluginManager().registerEvents(subListener, ConquerantPlugin.get());
                    this.subListeners.add(subListener);
                }
            }
        } catch (IOException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public <T extends SubListener> SubListener getClass(Class<T> clazz) {
        for (final SubListener subListener : this.subListeners) {
            if (subListener.getClass().equals(clazz))
                return subListener;
        }
        return null;
    }


}
