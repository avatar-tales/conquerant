package fr.avatarreturns.conquerant.listeners.editor;

import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CLocation;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.config.Permission;
import fr.avatarreturns.conquerant.listeners.manager.SubListener;
import fr.avatarreturns.conquerant.logs.Log;
import fr.avatarreturns.conquerant.utils.Pair;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class RegionCreator implements SubListener {

    private final Map<Player, Pair<Selector, List<CLocation>>> editors;

    public RegionCreator() {
        this.editors = new HashMap<>();
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(final PlayerInteractEvent e) {
        if (EquipmentSlot.HAND != e.getHand())
            return;
        if (e.getClickedBlock() == null)
            return;
        final ItemStack item = e.getItem();
        final Pair<Boolean, Selector> selector = this.hasCreator(e.getPlayer(), item);
        if (!selector.getFirst())
            return;
        e.setCancelled(true);
        if (!this.editors.containsKey(e.getPlayer()))
            this.editors.put(e.getPlayer(), Pair.from(selector.getSecond(), new ArrayList<>()));
        else if (this.editors.get(e.getPlayer()).getFirst() != selector.getSecond()) {
            this.editors.replace(e.getPlayer(), Pair.from(selector.getSecond(), new ArrayList<>()));
            Log.message(e.getPlayer(), Message.INFO_SELECTOR_CLEAR);
        }
        Message output;
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (selector.getSecond().equals(Selector.POLYGONAL)) {
                if (this.editors.get(e.getPlayer()).getSecond().size() == 0) {
                    Log.message(e.getPlayer(), Message.ERROR_SELECTOR_POLYGONAL_RIGHT);
                    return;
                }
                output = Message.INFO_SELECTOR_POLYGONAL_RIGHT;
            }else
                output = Message.INFO_SELECTOR_CUBOID_RIGHT;
        } else {
            if (selector.getSecond().equals(Selector.POLYGONAL)) {
                this.editors.get(e.getPlayer()).getSecond().clear();
                Log.message(e.getPlayer(), Message.INFO_SELECTOR_CLEAR);
                output = Message.INFO_SELECTOR_POLYGONAL_LEFT;
            } else
                output = Message.INFO_SELECTOR_CUBOID_LEFT;
        }
        final CLocation location = CAdapter.adapt(e.getClickedBlock().getLocation());
        this.editors.get(e.getPlayer()).getSecond().add(location);
        Log.message(e.getPlayer(), output.get(location.toStringBlock(), String.valueOf(this.editors.get(e.getPlayer()).getSecond().size())));
    }

    @SuppressWarnings({ "deprecation" })
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSneak(final PlayerToggleSneakEvent e) {
        if (!e.getPlayer().isOnGround() || e.getPlayer().isSneaking())
            return;
        final ItemStack item = e.getPlayer().getInventory().getItemInMainHand();
        final Pair<Boolean, Selector> selector = this.hasCreator(e.getPlayer(), item);
        if (!selector.getFirst())
            return;
        final ItemMeta meta = item.getItemMeta();
        assert meta.getLore() != null;
        Selector newSelector;
        if (selector.getSecond() == Selector.CUBOID) {
            meta.setLore(Collections.singletonList("  §7§l» §ePOLYGONAL"));
            newSelector = Selector.POLYGONAL;
        } else {
            meta.setLore(Collections.singletonList("  §7§l» §eCUBOID"));
            newSelector = Selector.CUBOID;
        }
        item.setItemMeta(meta);
        //e.getPlayer().getInventory().setItemInMainHand(item);
        Log.message(e.getPlayer(), Message.INFO_SELECTOR_CHANGE.get(selector.getSecond().name(), newSelector.name()));
        if (this.editors.containsKey(e.getPlayer())) {
            this.editors.remove(e.getPlayer());
            Log.message(e.getPlayer(), Message.INFO_SELECTOR_CLEAR);
        }
    }

    private Pair<Boolean, Selector> hasCreator(final Player player, final ItemStack item) {
        if (!Permission.SELECTOR_USE.hasPermission(player))
            return Pair.from(false, Selector.NONE);
        if (item == null)
            return Pair.from(false, Selector.NONE);
        if (item.getType() == Material.DIAMOND_PICKAXE) {
            if (!item.hasItemMeta())
                return Pair.from(false, Selector.NONE);
            final ItemMeta meta = item.getItemMeta();
            if (!meta.hasDisplayName() || !meta.hasLore())
                return Pair.from(false, Selector.NONE);
            if (meta.getDisplayName().equals("§cSELECTOR")) {
                assert meta.getLore() != null;
                if (meta.getLore().size() > 0) {
                    if (meta.getLore().get(0).equals("  §7§l» §eCUBOID"))
                        return Pair.from(true, Selector.CUBOID);
                    else
                        return Pair.from(true, Selector.POLYGONAL);
                }
            }
        }
        return Pair.from(false, Selector.NONE);
    }

    public Map<Player, Pair<Selector, List<CLocation>>> getEditors() {
        return this.editors;
    }

    public enum Selector {
        NONE,
        CUBOID,
        POLYGONAL
    }
}
