package fr.avatarreturns.conquerant.logs;

import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.plugin.ConquerantPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Log {

    public static void message(final CommandSender sender, final Message message) {
        message(sender, message.get(), true);
    }

    public static void message(final CommandSender sender, final Message message, final boolean consoleOutput) {
        message(sender, message.get(), consoleOutput);
    }

    public static void message(final CommandSender sender, final String message) {
        message(sender, message, true);
    }

    public static void message(final CommandSender sender, final String message, final boolean consoleOutput) {
        if (sender instanceof Player)
            sender.sendMessage(message);
        if (consoleOutput || !(sender instanceof Player))
            ConquerantPlugin.get().getServer().getConsoleSender().sendMessage((sender instanceof Player ? ChatColor.WHITE + "" + ChatColor.BOLD + "LOG FROM " + sender.getName() + ChatColor.GOLD + " »»»» " : "") + message);
    }
}
