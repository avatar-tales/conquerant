package fr.avatarreturns.conquerant.plugin;

import fr.avatarreturns.conquerant.command.manager.CommandManager;
import fr.avatarreturns.conquerant.config.Message;
import fr.avatarreturns.conquerant.config.Permission;
import fr.avatarreturns.conquerant.entities.adapted.CAdapter;
import fr.avatarreturns.conquerant.entities.adapted.objects.CPlayer;
import fr.avatarreturns.conquerant.entities.EditableEntity;
import fr.avatarreturns.conquerant.listeners.manager.ListeningManager;
import fr.avatarreturns.conquerant.utils.EnumerationIterable;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ConquerantPlugin extends JavaPlugin {

    public static ConquerantPlugin get() {
        return JavaPlugin.getPlugin(ConquerantPlugin.class);
    }

    private Map<Player, EditableEntity> editingEntities;

    @Override
    public void onEnable() {
        this.editingEntities = new HashMap<>();
        Message.init(this.getDataFolder().getAbsolutePath(), "messages.yml");
        Permission.init(this.getDataFolder().getAbsolutePath(), "permissions.yml");
        this.getServer().getOnlinePlayers().forEach(player ->
            CPlayer.getPlayers().add(CAdapter.adapt(player))
        );
        this.getCommand("conquerant").setExecutor(CommandManager.get());
        this.getCommand("conquerant").setTabCompleter(CommandManager.get());
        ListeningManager.get();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public Map<Player, EditableEntity> getEditingEntities() {
        return this.editingEntities;
    }

    public @NotNull File getFile() {
        return super.getFile();
    }

    public List<Class<?>> loadClasses(String basePackage, final String... subPackages) throws IOException {
        assert subPackages != null;
        final JarFile jar = new JarFile(ConquerantPlugin.get().getFile());
        for (int i = 0; i < subPackages.length; i++)
            subPackages[i] = subPackages[i].replace('.', '/') + "/";
        basePackage = basePackage.replace('.', '/') + "/";
        final List<Class<?>> result = new ArrayList<>();
        try {
            for (final JarEntry e : new EnumerationIterable<>(jar.entries())) {
                if (e.getName().startsWith(basePackage) && e.getName().endsWith(".class")) {
                    boolean load = subPackages.length == 0;
                    for (final String sub : subPackages) {
                        if (e.getName().startsWith(sub, basePackage.length())) {
                            load = true;
                            break;
                        }
                    }
                    if (load) {
                        final String c = e.getName().replace('/', '.').substring(0, e.getName().length() - ".class".length());
                        try {
                            result.add(Class.forName(c, true, ConquerantPlugin.get().getClass().getClassLoader()));
                        } catch (final ClassNotFoundException | ExceptionInInitializerError ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        } finally {
            try {
                jar.close();
            } catch (final IOException ignored) {}
        }
        return result;
    }
}
