package fr.avatarreturns.conquerant.config.storage;

public interface Storage {

    <T> void save(final T object);

    <T> T retrieve(final String attribute, final String name);

}
