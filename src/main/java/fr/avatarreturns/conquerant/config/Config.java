package fr.avatarreturns.conquerant.config;

public class Config {

    private static Config instance;

    public static Config get() {
        if (instance == null)
            instance = new Config();
        return instance;
    }

    private Config() {
    }
}
