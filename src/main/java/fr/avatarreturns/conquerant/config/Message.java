package fr.avatarreturns.conquerant.config;

import fr.avatarreturns.conquerant.utils.FileUtils;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Message {

    PREFIX_INFO("&3&lConquerant &8&l»"),
    PREFIX_ERROR("&c&lConquerant &4&l»"),
    PREFIX_WARN("&3&lConquerant &4&l»"),


    INFO_COMMAND_EXEC_CLAIM("%prefix_info% &7You claimed successfully the land &b{0}."),
    INFO_COMMAND_EXEC_EDIT("%prefix_info% &7You are editing &b{0}&7."),
    INFO_COMMAND_EXEC_GROUP_CREATE("%prefix_info% &7You have created the group &b{0}."),
    INFO_COMMAND_EXEC_INVITE_RECEIVE("%prefix_info% &7You received an invitation from &b{0} &7to join the group &b{1}&7."),
    INFO_COMMAND_EXEC_INVITE_SEND("%prefix_info% &7You just invite the player &b{0} &7into the group &b{1}&7."),
    INFO_COMMAND_EXEC_KICK_RECEIVE("%orefix_info% &7You have been kicked by &b{0} &7from the group &b{1}&7."),
    INFO_COMMAND_EXEC_KICK_SEND("%prefix_info% &7You kick the player &b{0} &7from the group &b{1}&7."),
    INFO_COMMAND_EXEC_LEAVE_SELF("%prefix_info% &7You left the guild &b{0}&7."),
    INFO_COMMAND_EXEC_LEAVE_OTHER("%prefix_info% &b{0} &7left the guild &b{0}&7."),
    INFO_COMMAND_EXEC_PROVINCE_ADD("%prefix_info% &7The land &b{0} &7is now a part of the province &b{1}&7."),
    INFO_PLAYER_JOIN_OTHERS("%prefix_info% &7The player &b{0} &7join the group &b{1}&7."),
    INFO_PLAYER_JOIN_SELF("%prefix_info% &7You join the group &b{0}&7."),
    INFO_PROVINCE_CREATED("%prefix_info% &7Province &b{0} &7created."),
    INFO_SELECTOR_CHANGE("%prefix_info% &7Your selector mode change from &b{0} &7to &b{1}&7."),
    INFO_SELECTOR_CLEAR("%prefix_info% &7Your former selection was cleared."),
    INFO_SELECTOR_CUBOID_LEFT("%prefix_info% &7Position 1 set to &b{0}&7."),
    INFO_SELECTOR_CUBOID_RIGHT("%prefix_info% &7Position 2 set to &b{0}&7."),
    INFO_SELECTOR_POLYGONAL_LEFT("%prefix_info% &7You start a new polygonal at &b{0}&7."),
    INFO_SELECTOR_POLYGONAL_RIGHT("%prefix_info% &7Add a point (&b#{1}&7) at &b{0}&7."),
    INFO_SELECTOR_RECEIVE("%prefix_info% &7You receive a magic selector."),


    ERROR_COMMAND_ARGUMENTS("%prefix_error% &cThe command &6/{0} &cneed &6{1} argument(s)."),
    ERROR_COMMAND_EXEC_CLAIM_LAND_CLAIMED("%prefix_error% &cThis land is already claimed."),
    ERROR_COMMAND_EXEC_CLAIM_LAND_INSIDE("%prefix_error% &cYou are not inside a land."),
    ERROR_COMMAND_EXEC_EDIT_EDITING("%prefix_error% &cYou already are editing an entity."),
    ERROR_COMMAND_EXEC_EDIT_ENTITY("%prefix_error% &cThis instance is already editing by someone else."),
    ERROR_COMMAND_EXEC_EDIT_NEDITING("%prefix_error% &cYou are not editing an entity."),
    ERROR_COMMAND_EXEC_EDIT_NPROVINCE("%prefix_error% &cYou are not editing a province"),
    ERROR_COMMAND_EXEC_INVITE_ALREADY("%prefix_error% &cThe player &6{0} &chas already an invitation from the group &6{1}&c."),
    ERROR_COMMAND_EXEC_INVITE_PRECISE("%prefix_error% &cYou have to precise a group to invite a player."),
    ERROR_COMMAND_EXECUTOR_PLAYER("%prefix_error% &cA player is required to execute the command &6/{0}&c."),
    ERROR_COMMAND_EXECUTOR_CONSOLE("%prefix_error% &cThe console is the only executor which can perform the command  &6/{0}&c."),
    ERROR_COMMAND_NEXISTS("%prefix_error% &cThe command &6/{0} &cdoes not exist."),
    ERROR_COMMAND_PERMISSION("%prefix_error% &cYou do not have the privilege to do that."),
    ERROR_COMMAND_UNKNOWN("%prefix_error% &cOoops, something went wrong"),
    ERROR_GROUP_EXISTS("%prefix_error% &cThe group &6{0} &calready exists."),
    ERROR_GROUP_MEMBER("%prefix_error% &cYou have to be at least a member of a group to perform this command."),
    ERROR_GROUP_NINSIDE("%prefix_error% &cYou are not a member of the group &6{0}&c."),
    ERROR_GROUP_NEXISTS("%prefix_error% &cThe group &6{0} &cdoes not exist."),
    ERROR_GROUP_PLAYER_NMEMBER("%prefix_error% &cThe player &6{0} &cis not a member of the group &6{0}&c."),
    ERROR_GROUP_RANK_POWER_INF("%prefix_error% &cYour rank power is not enough to do that on this player."),
    ERROR_GROUP_TYPE("%prefix_error% &cYou already are in a group with type &6{0}&c."),
    ERROR_PROVINCE_EXISTS("%prefix_error% &cA province with the name &6{0} &calready exists."),
    ERROR_PROVINCE_LAND_EXISTS("%prefix_error% &cThis province already has a land called &6{0}&c."),
    ERROR_PLAYER_NEXISTS("%prefix_error% &cThe player &6{0} &cdoes not exist."),
    ERROR_PLAYER_INVITATIONS("%prefix_error% &cNeed an invitation to join the group &6{0}&c."),
    ERROR_PLAYER_ONLINE("%prefix_error% &cThe player &6{0} &cis not online."),
    ERROR_PLAYER_SELF("%prefix_error% &cYou have to choose an other player than you."),
    ERROR_SELECTOR_NSET("%prefix_error% &cYou have to do a selection first."),
    ERROR_SELECTOR_POLYGONAL_RIGHT("%prefix_error% &cA start point is required. Use &6left click &cto set a start point in polygonal mode.");


    private String message;

    Message(final String message) {
        this.message = message;
    }

    public String getFullRaw() {
        return this.message;
    }

    public String get(final String... arguments) {
        String result = this.message;
        for (int i = 0; i < arguments.length; i++)
            result = result.replace("{" + i + "}", arguments[i]);
        return ChatColor.translateAlternateColorCodes('&', result
                .replace("%prefix_info%", PREFIX_INFO.message)
                .replace("%prefix_error%", PREFIX_ERROR.message)
                .replace("¤", " ")
        );
    }

    public static void init(final String path, final String name) {
        final File file = new File(path, name);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
            for (final Message messages : Message.values()) {
                if (messages.message.equals(""))
                    continue;
                final String pathToMessage = messages.name().replace("_", ".").toLowerCase();
                if (!FileUtils.addDefault(config, pathToMessage, messages.message)) {
                    messages.message = config.getString(pathToMessage);
                }
            }
            config.save(file);
        } catch (Exception ignored) {
        }
    }

}
